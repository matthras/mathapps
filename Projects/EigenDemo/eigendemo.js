// http://tube.geogebra.org/student/m135913

$(document).ready(function() {

	// Creating the initial graph box
	var brd = JXG.JSXGraph.initBoard('box1', {boundingbox: [-4, 4, 4, -4],axis: true});

	// Update function just to simply update the whole board with new values whenever any value is changed.
	update = function() {
		brd.update();
	};

	// Pauses regular updating so that we can add objects
	brd.suspendUpdate();

	// Creates the circle for the initial point/vector to be dragged around on.
	var inviscircle = brd.create('circle', [[0,0],1]);
	// Initial point P that is to be dragged around the circle
	var pt = brd.create('glider', [0, 1, inviscircle], {name: 'Drag me!'});
	// Line linking origin to P
	var ptvector = brd.create('line', [[0,0],pt],{straightFirst: false, straightLast: false, lastArrow: true});
	// Links P co-ordinates to text in HTML doc.
	brd.on('update',function() {document.getElementById('initVectorA').innerText = pt.X().toFixed(2);});
	brd.on('update',function() {document.getElementById('initVectorB').innerText = pt.Y().toFixed(2);});

	// P as transformed by the transformation matrix T as entered in the textboxes
	var transpt = brd.create('point', [function() {return pt.X() * document.getElementById('a').value + pt.Y() * document.getElementById('b').value;}, function() {return pt.X() * document.getElementById('c').value + pt.Y() * document.getElementById('d').value;}], {size: 3, name: ''});
	var transptvector = brd.create('line', [[0,0],transpt],{straightFirst: false, straightLast: false, lastArrow: true});
	brd.on('update',function() {document.getElementById('transVectorA').innerText = transpt.X().toFixed(2);});
	brd.on('update',function() {document.getElementById('transVectorB').innerText = transpt.Y().toFixed(2);});

	// Eigenvectors
	var eigen = numeric.eig([[document.getElementById('a').value,document.getElementById('b').value],[document.getElementById('c').value,document.getElementById('d').value]]);
	var eVectorPt1 = brd.create('point',[eigen.E.x[0][0],eigen.E.x[0][1]],{visible: false});
	var eVectorPt2 = brd.create('point',[eigen.E.x[1][0],eigen.E.x[1][1]],{visible: false});
	var eVector1 = brd.create('line',[[0,0],[eVectorPt1.X(),eVectorPt1.Y()]],{straightFirst: false, straightLast: false, lastArrow: true,visible: false});
	var eVector2 = brd.create('line',[[0,0],[eVectorPt2.X(),eVectorPt2.Y()]],{straightFirst: false, straightLast: false, lastArrow: true,visible: false});
	brd.unsuspendUpdate();

/* 	var dummypts = [];
	clickme = function() {
		brd.suspendUpdate();
		if(document.getElementById('cb').checked){
			dummypts.push(brd.create('point',[-2,-2]));
		}
		else {
			brd.removeObject(dummypts[dummypts.length-1])
			dummypts.splice(dummypts.length - 1, 1);
		}
		brd.unsuspendUpdate();
	}*/

	// Calculates eigenvalues + eigenvectors of T - configured to update when any number in T is changed.
	eigencalculate = function() {
		var eigen = numeric.eig([[document.getElementById('a').value,document.getElementById('b').value],[document.getElementById('c').value,document.getElementById('d').value]]);
		document.getElementById('lambda').innerText = eigen.lambda.x[0].toFixed(2) + " " + eigen.lambda.x[1].toFixed(2);
		document.getElementById('eigenvectors').innerText = "(" + parseFloat(eigen.E.x[0][0].toFixed(2)) + "," + parseFloat(eigen.E.x[0][1].toFixed(2)) + "), (" + parseFloat(eigen.E.x[1][0].toFixed(2)) + "," + parseFloat(eigen.E.x[1][1].toFixed(2)) + ")";
	};


	// Toggle showing of eigenvectors on graph
	var Evectors = [];
	showEvectors = function() {
		brd.suspendUpdate();
		if(document.getElementById('sEv').checked) {
			// var eigen = numeric.eig([[document.getElementById('a').value,document.getElementById('b').value],[document.getElementById('c').value,document.getElementById('d').value]]);
			// Evectors.push(brd.create('line',[[0,0],eigen.E.x[0]]));
			// Evectors.push(brd.create('line',[[0,0],eigen.E.x[1]]));
			eVectorPt1.setAttribute({visible: true});
			eVectorPt2.setAttribute({visible: true});
			eVector1.setAttribute({visible: true});
			eVector2.setAttribute({visible: true});
		}
		else {
			// brd.removeObject(Evectors[0]);
			// brd.removeObject(Evectors[1]);
			// Evectors = [];
			eVectorPt1.setAttribute({visible: false});
			eVectorPt2.setAttribute({visible: false});
			eVector1.setAttribute({visible: false});
			eVector2.setAttribute({visible: false});
		}
		brd.unsuspendUpdate();
	};

	// Toggle showing of basis vectors on graph
	var Bvectors = [];
	showBvectors = function () {
		brd.suspendUpdate();
		if(document.getElementById('sBv').checked) {
			Bvectors.push(brd.create('point',[function() {return document.getElementById('a').value;}, function(){return document.getElementById('c').value;}]));
			Bvectors.push(brd.create('point',[function() {return document.getElementById('b').value;}, function(){return document.getElementById('d').value;}]));
			// brd.on('update',function() {document.getElementById('a').value = Bvectors[0].X().toFixed(2);});
			// Bvectors.push(brd.create('line',[[0,0],[Bvectors[0].X(),Bvectors[0].Y()]], {straightFirst: false, straightLast: false, lastArrow: false}));
			// Bvectors.push(brd.create('line',[[0,0],[Bvectors[1].X(),Bvectors[1].Y()]], {straightFirst: false, straightLast: false, lastArrow: true}));
			//brd.on('update',function() {document.getElementById('a').value = Bvectors[0].X().toFixed(2);});
			//brd.addHook(function() {document.getElementById('c').value = Bvectors[0].Y().toFixed(2)});
			//brd.addHook(function() {document.getElementById('b').value = Bvectors[1].X().toFixed(2)});
			//brd.addHook(function() {document.getElementById('d').value = Bvectors[1].Y().toFixed(2)});
		}
		else {
			brd.removeObject(Bvectors[0]);
			brd.removeObject(Bvectors[1]);
			Bvectors = [];
		}
		brd.unsuspendUpdate();
	};
	// Recalculates basic vectors - configured to update when any number in T is changed.
	basicVectorMove = function () {
		if(document.getElementById('sBv').checked) { // Checks if the basic vectors are turned on or not
			Bvectors[0].moveTo([document.getElementById('a').value,document.getElementById('c').value]);
			Bvectors[1].moveTo([document.getElementById('b').value,document.getElementById('d').value]);
		}
	};
	// I wonder if it's necessary/worth it to write separate functions for a/c, b/d for optimisation purposes, since when you change a single value you don't need to update the other vector. That said, I'm just being lazy right now.

	// Math.transpose(math.multiply([[document.getElementById('a').value,document.getElementById('b').value],//[document.getElementById('c').value,document.getElementById('d').value]], [[v.X()], [v.Y()]])));

});
