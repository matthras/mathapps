$(document).ready( function() {
var brd = JXG.JSXGraph.initBoard('jxgbox', {
  axis: true,
  boundingbox: [-2, 1.5, 2, -1.5],
  keepaspectratio: true
});
var c = brd.create('circle', [
  [0, 0], 1
]);
var p = brd.create('glider', [-1, 0.5, c], {
  name: 'drag me'
}); // global variable

// Old way of linking the point to the value in the textbox
//brd.addHook(function() {
//  document.getElementById('degrees').value = (Math.atan2(p.Y(), p.X()) * 180 / Math.PI).toFixed(0);
//}, 'move');

// New way of linking point to value in textbox
brd.on('update',function() {document.getElementById('degrees').value = (Math.atan2(p.Y(), p.X()) * 180 / Math.PI).toFixed(0);});

setDirection = function() {
  var phi = 1 * document.getElementById('degrees').value * Math.PI / 180.0;
  var r = c.Radius();
  p.moveTo([r * Math.cos(phi), r * Math.sin(phi)],1000);

};
});
